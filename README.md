#Clone project

Open terminal/console and use
git clone https://YefremovDmitriy@bitbucket.org/YefremovDmitriy/cc-rwanda-team-2.-angular-8-frontend-tutorial.git

#Angular 8 Tutorial

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.0.

##Installation

Before to setup environment for Angular development using the Angular CLI tool, you must have installed Node.js on your system and set a development environment and npm package manager.

###Install Node.js

Angular requires Node.js version 10.9.0 or later. You can download it from [NodeJS](https://nodejs.org/en/)

After downloading, you have to install it on your system.


###Install Angular CLI

Use npm to Install Angular CLI
Use the following command to install Angular CLI

- npm install -g @angular/cli          OR
- npm install -g @angular/cli@latest       OR
- Just go to Angular CLI official website https://cli.angular.io/
You will see the whole cli command to create an Angular app. You need to run the first command to install Angular CLI. These steps are same for Windows and Mac.

###Check your Installed versions

To check Node and Angular CLI version, use 

- ng --version command.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

#Other Links

###Angular Documentation Reference

- [DOCS](https://angular.io/docs)
- [CLI](https://angular.io/cli)

###Bootstrap

Here is description for all Bootstrap 4 CSS Framework classes
https://www.w3schools.com/bootstrap4/bootstrap_ref_all_classes.asp

Installation npm package:
in root of project:
- npm install bootstrap --save
- then add in angular.json file next:
- "styles": [
   "src/styles.css",
   "./node_modules/bootstrap/dist/css/bootstrap.min.css"
 ],
 
###Some text redactors
- [VisualStudio](https://code.visualstudio.com/)
- [Atom](https://atom.io/)
