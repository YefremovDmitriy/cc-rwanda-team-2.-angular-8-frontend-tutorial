import { Component, OnInit } from '@angular/core';
import Product from '../Product';
import { ProductsService } from '../products.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-get',
  templateUrl: './product-get.component.html',
  styleUrls: ['./product-get.component.css']
})
export class ProductGetComponent implements OnInit {

  products: Product[];
  constructor(private route: ActivatedRoute, private router: Router, private ps: ProductsService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.ps.getProducts().subscribe((data: Product[]) => {
        this.products = data;
      });
    });
  }

  deleteProduct(id) {
    this.ps.deleteProduct(id).subscribe(res => {
      this.products.splice(id, 1);
    });
  }

}